package com.project.laundry.service;

import com.project.laundry.exception.NotFound;
import com.project.laundry.model.Customer;
import com.project.laundry.repository.CustomerServiceCustomQuery;
import com.project.laundry.repository.CustomerServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.*;

@Service
public class IrwansyahService {

    @Autowired
    private CustomerServiceRepository customerServiceRepository;
    @Autowired
    private CustomerServiceCustomQuery customerServiceCustomQuery;

    public Map save(Map pMap){
        Integer id = (Integer) pMap.get("customer_id");
        List<Map> dataList = (List<Map>) pMap.get("data_list");
        for(Map map : dataList){
            int productId = (Integer) map.get("product_id");
            String productName = (String) map.get("product_name");
            int qty = (Integer) map.get("qty");
            Map detail = (Map) map.get("details");
            if(detail == null){
                throw new NotFound("detailnya belum buat keynya wan ?");
            }
            String color = (String) detail.get("color");
            Integer size = (Integer) detail.get("size");
            Map subDetail = (Map) detail.get("sub_details");
            String subA =  (String) subDetail.get("sub_a");
            String subB =  (String) subDetail.get("sub_b");
        }
        return pMap;
    }
}
