package com.project.laundry.controller;

import com.project.laundry.service.CustomerService;
import com.project.laundry.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/save")
    public Map save(@RequestBody Map map){
       return customerService.save(map);
    }

    @GetMapping("/find_all_spec")
    public Map jpaSpec(@RequestParam(value = "page",  defaultValue = "0") int page,
                                          @RequestParam(value = "max",  defaultValue = "5") int maxsize,
                                          @RequestParam(value = "search",  defaultValue = "") String search){
        return customerService.findAll(page, maxsize, search);
    }

    @GetMapping("/find_all_custom_query")
    public Map customQuery(@RequestParam(value = "page",  defaultValue = "0") int page,
                           @RequestParam(value = "max",  defaultValue = "5") int maxsize,
                           @RequestParam(value = "search",  defaultValue = "") String search,
                           @RequestParam(value = "sort",  defaultValue = "true") Boolean sort) throws SQLException {
        return customerService.findAllCustomQuery(page, maxsize, search, sort);
    }
}
