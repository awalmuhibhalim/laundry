package com.project.laundry.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="customer_id")
    private Long customerId;

    @Column(name="name")
    private String name;

    @Column(name="birth_date")
    private Date birthDate;

    @Column(name="address")
    private String address;

    @Column(name="attachment")
    private String attachment;

    public Map getJson(){
        Map data = new HashMap();
        data.put("customer_id", getCustomerId());
        data.put("name", getName());
        data.put("birth_date", getBirthDate());
        data.put("address", getAddress());
        data.put("attachment", getAttachment());
        return data;
    }

}

