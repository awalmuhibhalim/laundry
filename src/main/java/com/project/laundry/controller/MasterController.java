package com.project.laundry.controller;

import com.project.laundry.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping
public class MasterController {
    @Autowired
    private MasterService masterService;

    @GetMapping("/get_master")
    public Map getMasters(){
       return masterService.getMaster();
    }
}
