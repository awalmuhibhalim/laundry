package com.project.laundry.service;

import com.project.laundry.exception.NotFound;
import com.project.laundry.model.Customer;
import com.project.laundry.repository.CustomerServiceCustomQuery;
import com.project.laundry.repository.CustomerServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.*;

@Service
public class CustomerService {

    @Autowired
    private CustomerServiceRepository customerServiceRepository;
    @Autowired
    private CustomerServiceCustomQuery customerServiceCustomQuery;

    public Map save(Map pMap){
        Integer id = (Integer) pMap.get("customer_id");
        String name = (String) pMap.get("name");
        Long birthDate = (Long) pMap.get("birth_date");
        String address = (String) pMap.get("address");
        String attachment = (String) pMap.get("attachment");

        Customer customer = new Customer();
        if(id != null){
            customer = customerServiceRepository.findById(id.longValue()).orElse(null);
            if(customer == null){
                throw new NotFound("data tidak ditemukan");
            }
        }
        customer.setName(name);
        customer.setBirthDate(new Date(birthDate.longValue()));
        customer.setAddress(address);
        customer.setAttachment(attachment);
        customer = customerServiceRepository.save(customer);
        return customer.getJson();
    }

    public Map findAll(Integer page, Integer maxSize, String pSearch) {
        String search = pSearch.toLowerCase();

        Specification spec1 = new Specification<Customer>() {
            @Override
            public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate predicate = cb.or(cb.like(cb.lower(root.get("name")), "" + search.toLowerCase() + "%"),
                        cb.like(cb.lower(root.get("address")), "" + search.toLowerCase() + "%"));
                return predicate;
            }
        };
        String sortTemp = "customerId";
        Sort sort = Sort.by(
                Sort.Order.desc(sortTemp));

        Pageable pageable = PageRequest.of(page, maxSize, sort);
        Page<Customer> pages = customerServiceRepository.findAll(spec1, pageable);
        List<Map> result = new ArrayList<>();
        for (Customer cs : pages.getContent()) {
            Map map = cs.getJson();
            result.add(map);
        }
        Map map = new HashMap();
        map.put("data", result);
        map.put("size", pages.getTotalElements());
        map.put("max", maxSize);
        map.put("total_page", pages.getTotalPages());
        map.put("page", (page == null ? 0 : page));
        map.put("timestamp", new Date().getTime());
        return map;
    }

    public Map findAllCustomQuery(Integer page, Integer maxSize, String pSearch, Boolean sort) throws SQLException {
        List<Map> result = new ArrayList<>();
        String search = pSearch.toLowerCase();
        Integer total = customerServiceCustomQuery.countCustomer(search);
        Integer total_page = total / maxSize;
        Integer start = 0;
        if (page > 0) {
            Integer tempStart = page;
            start = maxSize * tempStart;
        }
        List<Map<String, Object>> data = customerServiceCustomQuery.findAll(search, start, maxSize, sort);
        for (Map cs : data) {
            result.add(cs);
        }
        Map map = new HashMap();
        map.put("data", result);
        map.put("size", total);
        map.put("max", maxSize);
        map.put("total_page", total_page);
        map.put("page", (page == null ? 0 : page));
        map.put("timestamp", new Date().getTime());
        map.put("sort", sort);
        return map;
    }
}
