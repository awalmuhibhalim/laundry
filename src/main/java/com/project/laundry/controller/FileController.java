package com.project.laundry.controller;

import com.project.laundry.helper.FileUploadResponse;
import com.project.laundry.helper.FileUtils;
import com.project.laundry.service.FileService;
import com.project.laundry.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Controller
//@CrossOrigin()
@RequestMapping("/file")
public class FileController {
    @Autowired
    private FileService fileService;


    @PostMapping("/upload")
    public Map uploadFile(@RequestParam(value = "file") MultipartFile file) {
        String message = "";
        Map map = new HashMap();
//        try {
            fileService.createDirIfNotExist();
            fileService.upload(file);
//            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            map.put("message", message);
            return map;
    }

    @PostMapping("/multiple_upload")
    public ResponseEntity<FileUploadResponse> uploadFiles(@RequestParam("files") MultipartFile[] files) {
        try {
            System.out.println("testtttttttttttttttttttt");
            createDirIfNotExist();
            List<String> fileNames = new ArrayList<>();

            // read and write the file to the local folder
            Arrays.asList(files).stream().forEach(file -> {
                byte[] bytes = new byte[0];
                try {
                    bytes = file.getBytes();
                    Files.write(Paths.get(FileUtils.folderPath + file.getOriginalFilename()), bytes);
                    fileNames.add(file.getOriginalFilename());
                } catch (IOException e) {

                }
            });
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new FileUploadResponse("Files uploaded successfully: " + fileNames));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(new FileUploadResponse("Exception to upload files!"));
        }
    }

    @GetMapping("/download_report_customer")
    public ResponseEntity<Resource> downloadReportSurgery() throws Exception {
        String filename = "report_customer.xlsx";
        InputStreamResource file = new InputStreamResource(fileService.getFileXls());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/files")
    public ResponseEntity<String[]> getListFiles() {
        return ResponseEntity.status(HttpStatus.OK)
                .body( new File(FileUtils.folderPath).list());
    }

    private void createDirIfNotExist() {
        //create directory to save the files
        File directory = new File(FileUtils.folderPath);
        if (! directory.exists()){
            directory.mkdir();
        }
    }

}
