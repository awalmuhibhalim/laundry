package com.project.laundry.repository;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class CustomerServiceCustomQuery {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(CustomerServiceCustomQuery.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Map<String, Object>> findAll(String keyword, Integer start, Integer limit, Boolean sort) throws SQLException {
        String sorting = " customer_id DESC ";
        if(!sort){
            sorting = " customer_id ASC ";
        }
        String query =  "SELECT * FROM customer " +
                " WHERE lower(name) like '"+keyword+"%' OR lower(address) like '"+keyword+"%' " +
                " ORDER BY "+sorting+" LIMIT "+start+", "+limit+"";
        System.out.println("====================================== "+query);
        List<Map<String, Object>> result = jdbcTemplate.queryForList(query);
        jdbcTemplate.getDataSource().getConnection().close();
        return result;
    }

    public Integer countCustomer(String keyword) throws SQLException {
        String query =  "SELECT count(*) FROM customer " +
                " WHERE lower(name) like '"+keyword+"%' OR lower(address) like '"+keyword+"%' ";
        Integer count = jdbcTemplate.queryForObject(query, Integer.class);
        jdbcTemplate.getDataSource().getConnection().close();
        return count;
    }
}
