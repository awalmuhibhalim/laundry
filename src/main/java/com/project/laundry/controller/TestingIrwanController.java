package com.project.laundry.controller;

import com.project.laundry.service.CustomerService;
import com.project.laundry.service.IrwansyahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

@RestController
@RequestMapping("/irwansyah")
public class TestingIrwanController {
    @Autowired
    private IrwansyahService irwansyahService;

    @PostMapping("/save")
    public Map save(@RequestBody Map map){
       return irwansyahService.save(map);
    }

}
