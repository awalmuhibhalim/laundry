package com.project.laundry.service;

import ch.qos.logback.core.util.FileUtil;
import com.project.laundry.helper.FileUtils;
import com.project.laundry.model.Customer;
import com.project.laundry.repository.CustomerServiceRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class FileService {
    private final Path root = Paths.get("uploads");
    @Autowired
    private CustomerServiceRepository customerServiceRepository;

    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public void createDirIfNotExist() {
        //create directory to save the files
        File directory = new File(FileUtils.folderPath);
        if (! directory.exists()){
            directory.mkdir();
        }
    }

    public Map checkUpload(Map map){
        MultipartFile file = (MultipartFile) map.get("file");
        return upload(file);
    }
    public Map upload(MultipartFile file) {
        try {
//            init();
            byte[] bytes = new byte[0];
            bytes = file.getBytes();
            Files.write(Paths.get(FileUtils.folderPath + file.getOriginalFilename()), bytes);
            String fileNames = file.getOriginalFilename();
//            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        Map map = new HashMap();
        map.put("message", "success");
        return map;
    }

    public ByteArrayInputStream getFileXls() throws Exception {
        ByteArrayInputStream in = downloadExcelCustomer();
        return in;
    }

    public ByteArrayInputStream downloadExcelCustomer() {
        SimpleDateFormat sdfTtimestampFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        DateTimeFormatter dtfFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        String judul = "LAPORAN CUSTOMERS";
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            int currentRow = 0;
            // section title
            Sheet sheet = workbook.createSheet("Laporan");
            Row judulRow = sheet.createRow(currentRow++);
            Cell judulCell = judulRow.createCell(0);
            judulCell.setCellStyle(getTitleStyle(workbook));
            judulCell.setCellValue(judul);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 2));

            // header tabel
            String[] HEADERs = {"No", "Name", "Address"};
            int hRowIdx = currentRow++;
            Row hRow = sheet.createRow(hRowIdx);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = hRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }
            List<Map> data = getData();
            int dataRowIdx = hRowIdx;
            int no = 1;
            for (Map dp : data) {
                Row dataRow = sheet.createRow(++dataRowIdx);
                Cell dataNo = dataRow.createCell(0);
                dataNo.setCellStyle(getTableStyle(workbook));
                dataNo.setCellValue(no);
                no++;

                Cell cell1 = dataRow.createCell(1);
                cell1.setCellValue((String) dp.get("name"));
                cell1.setCellStyle(getTableStyle(workbook));

                Cell cell2 = dataRow.createCell(2);
                cell2.setCellValue((String) dp.get("address"));
                cell2.setCellStyle(getTableStyle(workbook));
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("err : " + e.getMessage());
        }
    }

    public List<Map> getData(){
        List<Customer> temp = customerServiceRepository.findByNameIsNotNull();
        List<Map> result = new ArrayList<>();
        for(Customer cs : temp){
            result.add(cs.getJson());
        }
        return result;
    }

    private static CellStyle getTitleStyle(Workbook wb) {
        XSSFFont boldFont= (XSSFFont) wb.createFont();
        boldFont.setFontHeightInPoints((short)10);
        boldFont.setFontName("Arial");
        boldFont.setColor(IndexedColors.BLACK.getIndex());
        boldFont.setBold(true);
        boldFont.setItalic(false);

        CellStyle style = wb.createCellStyle();
        style.setFont(boldFont);
        style.setAlignment(HorizontalAlignment.CENTER);

        return style;
    }

    private static CellStyle getTableStyle(Workbook wb) {
        XSSFFont defaultFont= (XSSFFont) wb.createFont();
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);

        CellStyle style = wb.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(defaultFont);
        return style;
    }
}
