package com.project.laundry.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MasterService {

    public Map getMaster(){
        Map result = new HashMap();
        List<Map> list = new ArrayList<>();
        Map map = new HashMap();
        map.put("id", 1);
        map.put("name", "Cuci");
        map.put("price", 5000);
        list.add(map);
        result.put("data", list);

        return result;
    }
}
