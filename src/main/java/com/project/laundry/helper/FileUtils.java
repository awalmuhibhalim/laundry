package com.project.laundry.helper;

import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public final class FileUtils {

    private FileUtils() {
        // restrict instantiation
    }

    public static final String folderPath =  "incoming-files//";
    public static final Path filePath = Paths.get(folderPath);

}
