package com.project.laundry.repository;

import com.project.laundry.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CustomerServiceRepository extends PagingAndSortingRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {
    Page<Customer> findAll(Specification specification, Pageable pageable);
    List<Customer> findByNameIsNotNull();
}
